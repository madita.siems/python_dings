import mysql.connector


# Getter
def getFirstName(knr):
	# Start connection
	connection = mysql.connector.connect(
		host="localhost",
		user="root",
		password="rootroot",
		database="projectdatabase"
	)

	# SQL Query
	cursor = connection.cursor()
	query = "SELECT VORNAME FROM KUNDE WHERE KUNDENNR = %s"
	cursor.execute(query % knr)
	result = cursor.fetchall()

	# Take first element from list from tuple
	return result[0][0]


# Getter
def getLastName(knr):
	# Start connection
	connection = mysql.connector.connect(
		host="localhost",
		user="root",
		password="rootroot",
		database="projectdatabase"
	)

	# SQL Query
	cursor = connection.cursor()
	query2 = "SELECT NACHNAME FROM KUNDE WHERE KUNDENNR = %s"
	cursor.execute(query2 % knr)
	result2 = cursor.fetchall()

	# Take first element from list from tuple
	return result2[0][0]


def showAllCustomers():
	# Start connection
	connection = mysql.connector.connect(
		host="localhost",
		user="root",
		password="rootroot",
		database="projectdatabase"
	)

	# SQL Query
	cursor = connection.cursor()
	sql_query = 'SELECT * FROM KUNDE'
	cursor.execute(sql_query)

	# Print table
	single_row = cursor.fetchone()
	while single_row is not None:
		print(single_row)
		single_row = cursor.fetchone()
	print(" ")

	# Close connection
	cursor.close()
	connection.close()


def addNewCustomer(knr, nn, vn, gd, str, hnr, plz, tel, mail):
	# Start connection
	connection = mysql.connector.connect(
		host="localhost",
		user="root",
		password="rootroot",
		database="projectdatabase"
	)

	print("Adding customer: " + knr, nn, vn, gd, str, hnr, plz, tel, mail)
	print(" ")

	# SQL Query
	cursor = connection.cursor()
	sql_query = 'INSERT INTO KUNDE (KUNDENNR, NACHNAME, VORNAME, GEBURTSDATUM, STRASSE, HAUSNR, PLZ, TELEFON, EMAIL) ' \
				'VALUES (' + knr + ', ' + nn + ', ' + vn + ', ' + gd + ', ' + str + ', ' + hnr + ', ' + plz + ', ' + tel + ', ' + mail + ')'
	cursor.execute(sql_query)
	connection.commit()
	showAllCustomers()

	# Close connection
	cursor.close()
	connection.close()


def deleteCustomer(knr):
	# Start connection
	connection = mysql.connector.connect(
		host="localhost",
		user="root",
		password="rootroot",
		database="projectdatabase"
	)

	last_name = getLastName(knr)
	first_name = getFirstName(knr)

	print("Deleting customer: " + first_name + " " + last_name)
	print(" ")

	# SQL Query
	cursor = connection.cursor()
	sql_query = 'DELETE FROM KUNDE WHERE KUNDENNR = ' + knr + ';'
	cursor.execute(sql_query)
	connection.commit()
	showAllCustomers()

	# Close connection
	cursor.close()
	connection.close()


def exportCustomerTable():
	# Start connection
	connection = mysql.connector.connect(
		host="localhost",
		user="root",
		password="rootroot",
		database="projectdatabase",
		auth_plugin='mysql_native_password'
	)

	# SQL Query
	cursor = connection.cursor()
	sql_query = 'SELECT * FROM KUNDE'
	cursor.execute(sql_query)

	# Write into .txt file
	file = open("text.txt", "w")
	row = cursor.fetchone()
	while row is not None:
		file.write(str(row) + "\n")
		row = cursor.fetchone()
	print("Exported customer table")

	# Close connection
	cursor.close()
	connection.close()

	# Only for test - shows that it works
	# for the running programm in command line start the commandLine.py
if __name__ == "__main__":
	showAllCustomers()
	addNewCustomer("'2010'", "'Mustermann'", "'Max'", "'1900-04-01'", "'Neue Strasse'", "'1'", "'20255'", "'040/9826434'", "'max.mustermann@web.de'")
	deleteCustomer("'2010'")
