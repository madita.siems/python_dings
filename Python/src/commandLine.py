import sys
from main import showAllCustomers, addNewCustomer, deleteCustomer, exportCustomerTable


def request():
	while True:
		number = input("What can I do for you? "
					   "0: Show all customers, "
					   "1: Export all customers, "
					   "2: Enter a new customer, "
					   "3: Delete an exitsting customer, "
					   "7: Exit, "
					   "8: Help"
					   )
		if number == "0":
			showAllCustomers()
		elif number == "1":
			exportCustomerTable()
		elif number == "2":
			requestForNewCustomer()
		elif number == "3":
			requestToDeleteCustomer()
		elif number == "7":
			sys.exit(0)
		elif number == "8":
			helpFunction()
		else:
			print("We don't have this number here!")


def requestForNewCustomer():
	print("Adding a new customer:")
	knr = "'" + input("What is the Id?") + "'"
	nn = "'" + input("What's the customers last name?") + "'"
	vn = "'" + input("What's the customers first name?") + "'"
	gd = "'" + input("When is the customers birthday? Please as following: YYYY-MM-DD.") + "'"
	str = "'" + input("What's the customers address?") + "'"
	hnr = "'" + input("What's the customers house number?") + "'"
	plz = "'" + input("What's the customers postcode?") + "'"
	tel = "'" + input("What's the customers phone number?") + "'"
	mail = "'" + input("What's the customers e-mail address?") + "'"
	addNewCustomer(knr, nn, vn, gd, str, hnr, plz, tel, mail)
	print("The customer was added!")


def requestToDeleteCustomer():
	print("Deleting a customer:")
	knr = "'" + input("What's the customers Id?") + "'"
	deleteCustomer(knr)
	print("The customer was deleted!")


def helpFunction():
	print("\n In this program you can simply manage your database. \n \n"
		  "Just enter a number to get either the table of all customers (1), export all customers (2),"
		  "enter a new customer (3), or delete an existing customer (4) \n")


if __name__ == "__main__":
	request()
