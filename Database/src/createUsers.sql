USE projectdatabase;

CREATE USER 'root'@'127.0.0.1' IDENTIFIED BY 'rootroot';

GRANT root TO 'root'@'127.0.0.1';

FLUSH PRIVILEGES;