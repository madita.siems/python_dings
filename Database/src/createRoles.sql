USE projectdatabase;

CREATE ROLE root;

GRANT ALL PRIVILEGES ON projectdatabase.* TO root;

FLUSH PRIVILEGES;
